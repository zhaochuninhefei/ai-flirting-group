本地尝试ChatGLM-6B
======

尝试在本地部署一个ChatGLM-6B，并尝试运行推理。

# 命令记录
```sh
# cd 到工作目录
cd xxx/jittor

# 国内使用 gitlink clone
git clone https://gitlink.org.cn/jittor/JittorLLMs.git --depth 1
# github: git clone https://github.com/Jittor/JittorLLMs.git --depth 1

cd JittorLLMs
# -i 指定用jittor的源， -I 强制重装Jittor版torch
pip install -r requirements.txt -i https://pypi.jittor.org/simple -I

# 拉取JittorLLMs项目
git clone https://gitlink.org.cn/jittor/JittorLLMs.git --depth 1

# 进入JittorLLMs目录
cd JittorLLMs/

# 安装JittorLLMs的依赖包 因为网络原因可能会超时失败，多试几次直到全部安装完成
pip install -r requirements.txt -i https://pypi.jittor.org/simple -I

# 下载并运行chatglm，这里会下载较长时间
python3 cli_demo.py chatglm

# 运行时报错，有些必须的依赖包如transformaer没有安装
# 执行安装，遇到超时失败就多试几次直到全部安装完成
python3 -m pip install -r models/chatglm/requirements.txt -i https://pypi.jittor.org/simple

# 全部安装成功后，重新运行, 这里因为本地只有一张3060(6G)显卡，因此指定了cpu使用内存上限与显存使用上限
export JT_SAVE_MEM=1
export cpu_mem_limit=18000000000
export device_mem_limit=5000000000
python3 cli_demo.py chatglm

# 安装gradio
pip install gradio -i https://pypi.jittor.org/simple -I
# 启动webui
python3 web_demo.py chatglm
```
