问题整理_LLM评测V2
=====

# 问题分类
将问题分为以下几类:
- 常识类问题
- 开放式问题
- 编程类问题
- 计算推理问题
- 创作类问题


# 常识类问题

1. 问：`中国历史上第一位皇帝和最后一位皇帝分别是谁？`，参考答案:`秦始皇嬴政/宣统帝溥仪`
2. 问：`二十四史的第一部是哪本书？作者是谁？`，参考答案:`《史记》司马迁`
3. 问：`“横眉冷对千夫指”的下句是什么？原诗是哪首？作者是谁？`，参考答案:`俯首甘为孺子牛，《自嘲》鲁迅(周树人)`
4. 问：`奥林匹克格言是什么？`，参考答案:`更高、更快、更强－－更团结(2021年加入"更团结")`
5. 问：`什么是“黄河夺淮”？`，参考答案:`黄河改道曾多次侵袭淮河流域，导致淮河失去入海口，排水不畅，水灾频繁。`
6. 问：`哈雷彗星多少年回归一次？`，参考答案:`大约75~76年`
7. 问：`在中国开车应该靠马路的哪一边行驶？`，参考答案:`右边`
8. 问：`人体正常体温大概是多少度左右？`，参考答案:`37摄氏度左右`
9. 问：`湖南和湖北的“湖”是哪个湖？`，参考答案:`洞庭湖`
10. 问：`“小熊猫”是小时候的大熊猫吗？`，参考答案:`不对`

# 开放式问题

1. 问：`搭乘手扶电梯时，大家应该都站到某一边还是随意站？`
2. 问：`哪个编程语言是世界上最好的编程语言？`
3. 问：`哲学三问到底在问什么？为什么说这三问是哲学或人生的终极三问？`
4. 问：`从目前的人工智能的表现来看，有没有可能出现人类之外的另一种智慧生命？`
5. 问：`如何评价美国的长臂管辖政策？`
6. 问：`“遥遥领先”是什么梗？`
7. 问：`请问这段话是什么意思？“想成功要做到五個字：「特別能吃苦」，而我想我快成功了，因为我已经做到了五分之四，「特別能吃」”`
8. 问：`纵观历史，是“英雄造时势”还是“时势造英雄”？`
9. 问：`请分析并评论计算机软件行业的开源与闭源之争。`
10. 问：`程序员的核心竞争力到底是什么？`

# 编程类问题

1. 问：`用bash实现一个大文件查找脚本，找出目标目录及其各级子目录下所有大小超过指定尺寸的文件。`
2. 问：`ssh登录到远程的linux服务器后，如何启动一个脚本任务才能确保即使关闭当前会话也不影响它继续执行，并能在下次登录时查看到该脚本任务的执行输出。`
3. 问：`用golang语言实现一个函数，满足需求：ssh连接目标服务器并远程执行命令。并提供该函数的test函数。`
4. 问：`用golang语言实现一个函数，满足需求：压缩目标目录为gzipped_tar_archive文件。并提供该函数的test函数。`
5. 问：`基于gin+gorm，给出一个多表联合查询的httpAPI的代码示例`
6. 问：`用java实现一段业务逻辑，获取发起http请求的客户端IP`
7. 问：`用java实现一段业务逻辑，实现两个不同数据类型的ArrayList之间的内联查询，内联字段是name`
8. 问：`基于springboot+mybatis，给出多表联合查询的httpAPI的代码示例`
9. 问：`请帮我设计一套权限管理相关的表，要求区分账户，角色，菜单，给出标准SQL的建表语句`
10. 问：`检查下面的java代码,解释它的作用并指出其中是否存在隐患:`
```
public class Waiter {
    protected int waitMaxTimes = 300;
    protected int waitMSPerTime = 1000;
    public static Waiter build() {
        return new Waiter();
    }
    public void sleepWhen(BooleanSupplier supplier) {
        int n = 0;
        while (supplier.getAsBoolean()) {
            if (n == waitMaxTimes) {
                throw new RuntimeException("处理超时");
            }
            try {
                Thread.sleep(waitMSPerTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            n++;
        }
    }
}
```


# 计算推理问题

1. 问：`桌子上有4个苹果，小红吃了1个，小刚拿走了2个，还剩下几个苹果？`
2. 问：`如果将红色和黄色混合在一起会是什么颜色？`
3. 问：`面朝北方，左转90度是什么方位？`
4. 问：`一公斤的棉花和一公斤的铁，哪一个更重？`
5. 问：`一只猫和一只鸡一共有几只脚？`
6. 问：`如果3x+1=10，那么x等于多少？`
7. 问：`序列1,4,9,16的后一个数是什么？`
8. 问：`小明爸爸有三个孩子,老大叫一毛,老二叫二毛,那老三叫啥?`
9. 问：`判断逻辑是否正确: 如果有些甲是乙，所有的丙都是乙，那么，一定有些甲是丙。`
10. 问：`一位农夫带着一头狼，一只羊和一筐白菜过河，河边有一条小船，农夫划船每次只能载狼、羊、白菜三者中的一个过河。农夫不在旁边时，狼会吃羊，羊会吃白菜。问农夫该如何过河。`

# 创作类问题

1. 问题：`请你帮我写一个介绍GPT的PPT大纲`
2. 问题：`请你帮我写一封给所有程序员的信，主题是感谢程序员群体`
3. 问题：`我得了新冠，请帮我写一封假条。`
4. 问题：`我们研发了一款家教助手APP，请你帮我写一段广告词`
5. 问题：`你是一个主持人，请你为“十佳开源项目”颁奖礼写一段开场词。`
6. 问题：`帮我写一个潮汕养生牛肉火锅店的探店点评`
7. 问题：`仿照徐志摩的风格，写一首以赞美春天为主题的现代诗。`
8. 问题：`仿照鲁迅的口吻，写一篇杂文讽刺美国的技术出口管制政策。`
9. 问题：`仿照古龙的风格，写一段文字描述楚留香和陆小凤的比武场景。`
10. 问题：`请以"我抢走了皇帝的女人"为开头写一篇悬疑风格短篇小说。`
