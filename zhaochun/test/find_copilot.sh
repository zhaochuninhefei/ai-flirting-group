#!/bin/bash
# 用 bash 实现对指定目录的完整遍历，打印出所有的子目录与文件
# 用法： bash traverse.sh /path/to/directory

# 检查参数是否为一个有效的目录
if [ ! -d "$1" ]; then
  echo "请输入一个有效的目录作为参数"
  exit 1
fi

# 用 find 命令遍历目录，打印出所有的子目录和文件
find "$1" -print