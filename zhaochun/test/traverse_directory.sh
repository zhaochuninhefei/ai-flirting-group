#!/bin/bash

function traverse_directory() {
    local directory="$1"
    local indent="${2:-""}"
    
    # 打印目录名称
    echo "${indent}${directory}/"
    
    # 遍历目录中的文件和子目录
    for item in "$directory"/*; do
        if [[ -d "$item" ]]; then
            # 递归遍历子目录
            traverse_directory "$item" "$indent  "
        else
            # 打印文件名称
            echo "${indent}$(basename "$item")"
        fi
    done
}

# 指定要遍历的目录
target_directory="./"

# 调用函数进行遍历
traverse_directory "$target_directory"