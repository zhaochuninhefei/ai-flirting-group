#!/bin/bash

set -e
  
# 遍历指定目录下的所有子目录  
function traverse_dir {  
  if [ $# -eq 0 ]; then  
    # 当只传递了目录名时，递归遍历该目录  
    dir="$1"  
    if [ -d "$dir" ]; then  
      traverse_dir "$dir"  
    else  
      echo "无法找到 $dir"  
    fi  
  else  
    # 当有参数时，递归遍历该目录下的所有子目录  
    dir="$1"  
    for subdir in "$dir"/*; do  
      traverse_dir "$subdir"  
    done  
  fi  
}  
  
# 测试遍历函数  
traverse_dir ~/work/sources/gitee.com/zhaochuninhefei/ai-flirting-group/zhaochun/test