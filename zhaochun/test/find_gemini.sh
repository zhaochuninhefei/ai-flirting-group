#!/bin/bash

# 定义要遍历的目录
dir="/home/zhaochun/work/sources/gitee.com/zhaochuninhefei/ai-flirting-group/zhaochun/test/testdir"

# 使用 for 循环遍历目录下的所有文件和目录
for entry in "$dir"/*; do
  # 判断当前项是文件还是目录
  if [ -d "$entry" ]; then
    # 如果是目录，则递归遍历该目录
    echo "目录：$entry"
    fun_directory "$entry"
  else
    # 如果是文件，则打印文件名
    echo "文件：$entry"
  fi
done

# 定义递归遍历目录的函数
fun_directory() {
  local dir="$1"

  # 遍历目录下的所有文件和目录
  for entry in "$dir"/*; do
    # 判断当前项是文件还是目录
    if [ -d "$entry" ]; then
      # 如果是目录，则递归遍历该目录
      echo "目录：$entry"
      fun_directory "$entry"
    else
      # 如果是文件，则打印文件名
      echo "文件：$entry"
    fi
  done
}