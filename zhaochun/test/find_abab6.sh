#!/bin/bash

# 指定要遍历的目录
dir_to_search="$1"

# 检查目录是否存在
if [ ! -d "$dir_to_search" ]; then
    echo "目录不存在: $dir_to_search"
    exit 1
fi

# 使用find命令遍历目录
find "$dir_to_search" -mindepth 1 -type f -print0 | while IFS= read -r -d '' file; do
    echo "文件: $file"
done

find "$dir_to_search" -mindepth 1 -type d -print0 | while IFS= read -r -d '' dir; do
    echo "子目录: $dir"
done