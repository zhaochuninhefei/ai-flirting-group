#!/bin/bash

# 定义遍历函数
traverse() {
    local dir="$1"
    
    # 遍历当前目录下的所有文件和目录
    for entry in "$dir"/*; do
        # 如果是目录,则递归调用自己
        if [ -d "$entry" ]; then
            echo "Directory: $entry"
            traverse "$entry"
        # 如果是文件,则打印文件名
        elif [ -f "$entry" ]; then
            echo "File: $entry"
        fi
    done
}

# 检查是否提供了目录参数
if [ -z "$1" ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# 调用遍历函数
traverse "$1"