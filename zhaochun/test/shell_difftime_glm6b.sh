#!/bin/bash

# 获取当前时间戳
now=$(date +"%Y-%m-%d %H:%M:%S")

# 输出当前时间戳
echo "当前时间戳： $now"

date -d "$now" +%s

# 计算两个时间戳之间的秒数差距
diff_seconds=$(date -d "$now" +%s -d "$now" +%s)

# 输出结果
echo "两个时间戳之间的秒数差距： $diff_seconds"
