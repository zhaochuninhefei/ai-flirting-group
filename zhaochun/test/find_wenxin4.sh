#!/bin/bash

# 遍历目录函数
traverse_dir() {
    local dir="$1"
    # 遍历目录中的文件和子目录
    for entry in "$dir"/*; do
        if [[ -f "$entry" ]]; then
            # 如果是文件，则打印文件名
            echo "文件: $entry"
        elif [[ -d "$entry" ]]; then
            # 如果是目录，则打印目录名并递归遍历
            echo "目录: $entry"
            traverse_dir "$entry"
        fi
    done
}

# 检查命令行参数
if [[ $# -ne 1 ]]; then
    echo "用法: $0 <目录>"
    exit 1
fi

# 调用遍历目录函数
traverse_dir "$1"