#!/bin/bash

# 指定目录
dir="./"

# 使用find命令遍历指定目录下的所有文件和子目录，并通过管道传递给ls命令进行处理
find "$dir" -type f | while read file; do
  echo "$file"
done

find "$dir" -type d | while read dir; do
  echo "$dir"
done